# house-price-prediction-using-regression

Predicting the sale price of the houses using regression algorithms. Housing sales price are determined by numerous factors such as area of the property, location of the house, material used for construction and so on.